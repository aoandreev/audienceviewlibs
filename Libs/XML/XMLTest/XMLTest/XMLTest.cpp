
#include "stdafx.h"
#include <iostream>

#define ASCII_FILE_NAME "C:/SHS/AudienceView/Libs/XML/samples/asciixmlsample.xml"
#define U16_FILE_NAME "C:/SHS/AudienceView/Libs/XML/samples/xmlsample.xml"
static std::string u8_file_content =
"<personlist company = \"SHS\" name = \"stuff\">\n"
"<person number = \"1\" type = \"ascii\">qqqqqqqqqqqqqqqqqqqq< / person>\n"
"<person number = \"2\" type = \"ascii\">wwwwwwwwwwwwwwwwwwww< / person>\n"
"<person number = \"3\" type = \"ascii\">eeeeeeeeeeeeeeeeeeee< / person>\n"
"< / personlist>";

static std::u16string u16_file_content =
u"<personlist company = \"SHS\" name = \"stuff\">\n"
u"<person number = \"1\" type = \"ascii\">qqqqqqqqqqqqqqqqqqqq< / person>\n"
u"<person number = \"2\" type = \"utf16\">∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯< / person>\n"
u"<person number = \"3\" type = \"utf32\">潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮< / person>\n"
u"< / personlist>";

////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////UTILS////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////#include <cstdlib>
//#include <codecvt> 
//#include <locale> 
//std::u16string utf8_to_utf16(const std::string &s) {
//	std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> convert;
//	return convert.from_bytes(s);
//}
//
//std::string utf16_to_utf8(const std::u16string &s) {
//	std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> convert;
//	return convert.to_bytes(s);
//}
//
//
//
//std::u16string read_file()
//{
//	typedef std::istreambuf_iterator<char> iter;
//	std::ifstream file;
//	file.open(U16_FILE_NAME);
//	if (file.is_open())
//	{
//		u8_file_content = std::string((iter(file)), iter());
//		file.close();
//		u16_file_content = utf8_to_utf16(u8_file_content);
//	}
//	return u16_file_content;
//}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////


#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/parsers/SAXParser.hpp>


using namespace std;
using namespace xerces;

//int main(int argc, char* args[]) {
//
//	try {
//		XMLPlatformUtils::Initialize();
//	}
//	catch (const XMLException& toCatch) {
//		char* message = XMLString::transcode(toCatch.getMessage());
//		cout << "Error during initialization! :\n"
//			<< message << "\n";
//		XMLString::release(&message);
//		return 1;
//	}
//
//	XercesDOMParser* parser = new XercesDOMParser();
//	parser->setValidationScheme(XercesDOMParser::Val_Always);
//	parser->setDoNamespaces(true);    // optional
//
//	ErrorHandler* errHandler = (ErrorHandler*) new HandlerBase();
//	parser->setErrorHandler(errHandler);
//	char* xmlFile = "x1.xml";
//
//	try {
//		parser->parse(xmlFile);
//	}
//	catch (const XMLException& toCatch) {
//		char* message = XMLString::transcode(toCatch.getMessage());
//		cout << "Exception message is: \n"
//			<< message << "\n";
//		XMLString::release(&message);
//		return -1;
//	}
//	catch (const DOMException& toCatch) {
//		char* message = XMLString::transcode(toCatch.msg);
//		cout << "Exception message is: \n"
//			<< message << "\n";
//		XMLString::release(&message);
//		return -1;
//	}
//	catch (...) {
//		cout << "Unexpected Exception \n";
//		return -1;
//	}
//
//	delete parser;
//	delete errHandler; try {
//		XMLPlatformUtils::Initialize();
//	}
//	catch (const XMLException& toCatch) {
//		char* message = XMLString::transcode(toCatch.getMessage());
//		cout << "Error during initialization! :\n"
//			<< message << "\n";
//		XMLString::release(&message);
//		return 1;
//	}
//
//	XercesDOMParser* parser = new XercesDOMParser();
//	parser->setValidationScheme(XercesDOMParser::Val_Always);
//	parser->setDoNamespaces(true);    // optional
//
//	ErrorHandler* errHandler = (ErrorHandler*) new HandlerBase();
//	parser->setErrorHandler(errHandler);
//	char* xmlFile = "x1.xml";
//
//	try {
//		parser->parse(xmlFile);
//	}
//	catch (const XMLException& toCatch) {
//		char* message = XMLString::transcode(toCatch.getMessage());
//		cout << "Exception message is: \n"
//			<< message << "\n";
//		XMLString::release(&message);
//		return -1;
//	}
//	catch (const DOMException& toCatch) {
//		char* message = XMLString::transcode(toCatch.msg);
//		cout << "Exception message is: \n"
//			<< message << "\n";
//		XMLString::release(&message);
//		return -1;
//	}
//	catch (...) {
//		cout << "Unexpected Exception \n";
//		return -1;
//	}
//
//	DOMDocument* doc = parser->getDocument();
//
//	DOMNamedNodeMap* attrs = nullptr;
//	DOMNode* curr_node = nullptr;
//	while (curr_node = doc->getNextSibling()) 
//	{
//		curr_node
//	
//	}
//
//	doc->getAttributes();
//
//	delete parser;
//	delete errHandler;
//	return 0;
//}


void write(DOMNode* node);
void writeElement(DOMElement* element);
void writeText(DOMText* text);

void
write(DOMNode* node) {
	if (node) {
		switch (node->getNodeType()) {
		case DOMNode::ELEMENT_NODE:
			writeElement(static_cast<DOMElement*>(node));
			break;
		case DOMNode::TEXT_NODE:
			writeText(static_cast<DOMText*>(node));
			break;
		}

		DOMNode* child = node->getFirstChild();
		while (child) {
			DOMNode* next = child->getNextSibling();
			write(child);
			child = next;
		}
	}
}

void
writeElement(DOMElement* element) {
	char* name = XMLString::transcode(element->getTagName());
	cout << "tag    : " << name << endl;
	XMLString::release(&name);

	DOMNamedNodeMap* map = element->getAttributes();
	for (XMLSize_t i = 0; i < map->getLength(); i++) {
		DOMAttr* attr = static_cast<DOMAttr*>(map->item(i));
		char* attr_name = XMLString::transcode(attr->getName());
		char* attr_value = XMLString::transcode(attr->getValue());
		cout << attr_name << ": " << attr_value << endl;

		XMLString::release(&attr_name);
		XMLString::release(&attr_value);
	}
}

void
writeText(DOMText* text) {
	XMLCh* buffer = new XMLCh[XMLString::stringLen(text->getData()) + 1];
	XMLString::copyString(buffer, text->getData());
	XMLString::trim(buffer);
	char* content = XMLString::transcode(buffer);
	delete[] buffer;

	cout << "content: " << content << endl;
	XMLString::release(&content);
}

int main(int argc, char* args[]) {
	std::cout << std::dec << << " ";

	try {
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		cout << "Error during initialization! :\n" << message << "\n";
		XMLString::release(&message);
		return 1;
	}

	XercesDOMParser* parser = new XercesDOMParser();
	parser->setValidationScheme(XercesDOMParser::Val_Always);
	parser->setDoNamespaces(true); // optional

	ErrorHandler* errHandler = (ErrorHandler*) new HandlerBase();
	parser->setErrorHandler(errHandler);



	try {
		u16_file_content.data();
		MemBufInputSource myxml_buf((XMLByte*)(u16_file_content.c_str()), u8_file_content.size()*2,
			"myxml (in memory)");
		myxml_buf.fSrcBytes;
		myxml_buf.getEncoding();
		myxml_buf.setEncoding("en_US");
		parser->parse(myxml_buf);//parser->parse((ASCII_FILE_NAME);
		std::string u8_content;
		const char* p_8stf = u8_content.c_str();
		parser->parse((const char*)p_8stf)

		DOMDocument* dom = parser->getDocument();
		write(dom);
	}
	catch (const XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		cout << "Exception message is: \n" << message << "\n";
		XMLString::release(&message);
		return -1;
	}
	catch (const DOMException& toCatch) {
		char* message = XMLString::transcode(toCatch.msg);
		cout << "Exception message is: \n" << message << "\n";
		XMLString::release(&message);
		return -1;
	}
	catch (...) {
		cout << "Unexpected Exception \n";
		return -1;
	}

	delete parser;
	delete errHandler;
	return 0;
}

