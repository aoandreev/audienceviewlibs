# CMake generated Testfile for 
# Source directory: C:/SHS/AudienceView/Libs/XML/Libs/xerces-c-3.2.1
# Build directory: C:/SHS/AudienceView/Libs/XML/Libs/xerces-c-3.2.1
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("doc")
subdirs("src")
subdirs("tests")
subdirs("samples")
