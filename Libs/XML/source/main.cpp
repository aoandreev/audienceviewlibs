﻿#include <iostream>
#include <string>
#include <string.h>
#include <cstdlib>
#include <codecvt> 
#include <locale> 

#define ASCII_FILE_NAME "samples/asciixmlsample.xml"
#define U16_FILE_NAME "samples/xmlsample.xml"
static std::string u8_file_content;
static std::u16string u16_file_content;
static std::u32string u32_file_content;


//static std::string u8_content =
//"<personlist company=\"SHS\" name=\"stuff\">"
//"<person number=\"1\" type=\"ascii\">qqqqqqqqqqqqqqqqqqqq</person>"
//"<person number=\"2\" type=\"ascii\">wwwwwwwwwwwwwwwwwwww</person>"
//"<person number=\"3\" type=\"ascii\">eeeeeeeeeeeeeeeeeeee</person>"
//"</personlist>";

static std::u16string u16_content =
u"<personlist company=\"SHS\" name=\"stuff\">"
u"<person number=\"1\" type=\"ascii\">qqqqqqqqqqqqqqqqqqqq</person>"
u"<person number=\"2\" type=\"utf16\">∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯</person>"
u"<person number=\"3\" type=\"utf16\">∯∯q∯∯q∯∯as∯∯∯ewq∯∯∯∯∯ax∯∯∯∯∯∯</person>"
u"<person number=\"4\" type=\"utf16\">ⰧⰧⰧⰧⰧⰧⰧⰧⰧⰧⰧⰧⰧⰧⰧ</person>"
u"<person number=\"5\" type=\"utf16\">ⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯⲯ</person>"
u"<person number=\"6\" type=\"utf32\">潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮</person>"
u"</personlist>";

static std::string u8_content = "<person>qqqqqqqqqqqqqqqqqqqq</person>";
//static std::u16string u16_content = u"<person>qqqqqqqqqqqqqqqqqqqq</person>\0";

//static std::u16string u16_content = u"<person>∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯</person>";


std::u16string utf8_to_utf16(const std::string &s) {
	std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> convert;
	return convert.from_bytes(s);
}

std::string utf16_to_utf8(const std::u16string &s) {
	std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> convert;
	return convert.to_bytes(s);
}


//std::u16string read_file()
//{
//	typedef std::istreambuf_iterator<char> iter;
//	std::ifstream file;
//	file.open(U16_FILE_NAME);
//	if (file.is_open())
//	{
//		u8_file_content = std::string((iter(file)), iter());
//		file.close();
//		u16_file_content = utf8_to_utf16(u8_file_content);
//		u32_file_content = utf8_to_utf32(u8_file_content);
//	}
//	return u16_file_content;
//}


#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/parsers/SAXParser.hpp>
#include <xercesc/parsers/DOMLSParserImpl.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/Wrapper4InputSource.hpp>

using namespace std;
using namespace xercesc;

void PrintLog(int _step, std::string _entrie = "")
{
	std::cout << "Step " << _step;
	if (!_entrie.empty())
	{
		std::cout << ": " << _entrie;
	}
	std::cout << std::endl;
}


void write(DOMNode* node);
void writeElement(DOMElement* element);
void writeText(DOMText* text);

void
write(DOMNode* node) {
	if (node) {
		switch (node->getNodeType()) {
		case DOMNode::ELEMENT_NODE:
			writeElement(static_cast<DOMElement*>(node));
			break;
		case DOMNode::TEXT_NODE:
			writeText(static_cast<DOMText*>(node));
			break;
		}

		DOMNode* child = node->getFirstChild();
		while (child) {
			DOMNode* next = child->getNextSibling();
			write(child);
			child = next;
		}
	}
}

void writeElement(DOMElement* element) 
{
	std::u16string name(element->getTagName());
	std::cout << "tag    : " << utf16_to_utf8(name) << endl;

	DOMNamedNodeMap* map = element->getAttributes();
	for (XMLSize_t i = 0; i < map->getLength(); i++) {
		DOMAttr* attr = static_cast<DOMAttr*>(map->item(i));
		std::u16string attr_name(attr->getName());
		std::u16string attr_value(attr->getValue());
		cout << utf16_to_utf8(attr_name) << ": " << utf16_to_utf8(attr_value) << endl;
	}
}

void writeText(DOMText* text) 
{
	std::u16string content(text->getData());
	cout << "content: " << utf16_to_utf8(content) << endl;
}

int main(int argc, char* args[]) {

	std::cout << ">>>>>>>>>> USING UTF-16 CONTENT <<<<<<<<<<" << std::endl;
	try {
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		cout << "Error during initialization! :\n" << message << "\n";
		XMLString::release(&message);
		return 1;
	}

	XercesDOMParser *parser = new XercesDOMParser();
	ErrorHandler* errHandler = (ErrorHandler*) new HandlerBase();
	parser->setErrorHandler(errHandler);

	try {

		/////////////////////////////////////////////////////////////////////////
		MemBufInputSource myxml_buf((const XMLByte*)(u16_content.c_str()), u16_content.length() /*sizeof(char16_t) = 2*/* 2, "dummy", false);
		myxml_buf.setEncoding(u"UTF-16");//NECESSARY
		parser->parse(myxml_buf);
		/////////////////////////////////////////////////////////////////////////

		DOMDocument* dom = parser->getDocument();
		write(dom);
	}
	catch (const XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		cout << "Exception message is: \n" << message << "\n";
		XMLString::release(&message);
		return -1;
	}
	catch (const DOMException& toCatch) {
		char* message = XMLString::transcode(toCatch.msg);
		cout << "Exception message is: \n" << message << "\n";
		XMLString::release(&message);
		return -1;
	}
	catch (...) {
		cout << "Unexpected Exception \n";
		return -1;
	}

	delete parser;
	delete errHandler;
	return 0;
}