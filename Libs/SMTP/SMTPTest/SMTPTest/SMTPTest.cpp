#include <iostream>
#include <string> 
#include <codecvt> 

#include "vmime/vmime.hpp"
#include "vmime/platforms/posix/posixHandler.hpp"

////USEFULL CLASSES

#if VMIME_HAVE_SASL_SUPPORT

// SASL authentication handler
class interactiveAuthenticator : public vmime::security::sasl::defaultSASLAuthenticator
{
	const std::vector <vmime::shared_ptr <vmime::security::sasl::SASLMechanism> > getAcceptableMechanisms
	(const std::vector <vmime::shared_ptr <vmime::security::sasl::SASLMechanism> >& available,
		vmime::shared_ptr <vmime::security::sasl::SASLMechanism> suggested) const
	{
		std::cout << std::endl << "Available SASL mechanisms:" << std::endl;

		for (unsigned int i = 0; i < available.size(); ++i)
		{
			std::cout << "  " << available[i]->getName();

			if (suggested && available[i]->getName() == suggested->getName())
				std::cout << "(suggested)";
		}

		std::cout << std::endl << std::endl;

		return defaultSASLAuthenticator::getAcceptableMechanisms(available, suggested);
	}

	void setSASLMechanism(vmime::shared_ptr <vmime::security::sasl::SASLMechanism> mech)
	{
		std::cout << "Trying '" << mech->getName() << "' authentication mechanism" << std::endl;

		defaultSASLAuthenticator::setSASLMechanism(mech);
	}

	const vmime::string getUsername() const
	{
		if (m_username.empty())
			m_username = getUserInput("Username");

		return m_username;
	}

	const vmime::string getPassword() const
	{
		if (m_password.empty())
			m_password = getUserInput("Password");

		return m_password;
	}

	static const vmime::string getUserInput(const std::string& prompt)
	{
		std::cout << prompt << ": ";
		std::cout.flush();

		vmime::string res;
		std::getline(std::cin, res);

		return res;
	}

private:

	mutable vmime::string m_username;
	mutable vmime::string m_password;
};

#else // !VMIME_HAVE_SASL_SUPPORT

// Simple authentication handler
class interactiveAuthenticator : public vmime::security::defaultAuthenticator
{
	const vmime::string getUsername() const
	{
		if (m_username.empty())
			m_username = getUserInput("Username");

		return m_username;
	}

	const vmime::string getPassword() const
	{
		if (m_password.empty())
			m_password = getUserInput("Password");

		return m_password;
	}

	static const vmime::string getUserInput(const std::string& prompt)
	{
		std::cout << prompt << ": ";
		std::cout.flush();

		vmime::string res;
		std::getline(std::cin, res);

		return res;
	}

private:

	mutable vmime::string m_username;
	mutable vmime::string m_password;
};

#endif // VMIME_HAVE_SASL_SUPPORT

#if VMIME_HAVE_TLS_SUPPORT

// Certificate verifier (TLS/SSL)
class interactiveCertificateVerifier : public vmime::security::cert::defaultCertificateVerifier
{
public:

	void verify(vmime::shared_ptr <vmime::security::cert::certificateChain> chain, const vmime::string& hostname)
	{
		try
		{
			setX509TrustedCerts(m_trustedCerts);

			defaultCertificateVerifier::verify(chain, hostname);
		}
		catch (vmime::security::cert::certificateException&)
		{
			// Obtain subject's certificate
			vmime::shared_ptr <vmime::security::cert::certificate> cert = chain->getAt(0);

			std::cout << std::endl;
			std::cout << "Server sent a '" << cert->getType() << "'" << " certificate." << std::endl;
			std::cout << "Do you want to accept this certificate? (Y/n) ";
			std::cout.flush();

			std::string answer;
			std::getline(std::cin, answer);

			if (answer.length() != 0 &&
				(answer[0] == 'Y' || answer[0] == 'y'))
			{
				// Accept it, and remember user's choice for later
				if (cert->getType() == "X.509")
				{
					m_trustedCerts.push_back(vmime::dynamicCast
						<vmime::security::cert::X509Certificate>(cert));

					setX509TrustedCerts(m_trustedCerts);
					defaultCertificateVerifier::verify(chain, hostname);
				}

				return;
			}

			throw vmime::security::cert::certificateException
			("User did not accept the certificate.");
		}
	}

private:

	static std::vector <vmime::shared_ptr <vmime::security::cert::X509Certificate> > m_trustedCerts;
};


std::vector <vmime::shared_ptr <vmime::security::cert::X509Certificate> >
interactiveCertificateVerifier::m_trustedCerts;

#endif // VMIME_HAVE_TLS_SUPPORT

/** Time out handler.
* Used to stop the current operation after too much time, or if the user
* requested cancellation.
*/

#include <ctime>

class timeoutHandler : public vmime::net::timeoutHandler
{
public:

	timeoutHandler()
		: m_start(time(NULL))
	{
	}

	bool isTimeOut()
	{
		// This is a cancellation point: return true if you want to cancel
		// the current operation. If you return true, handleTimeOut() will
		// be called just after this, and before actually cancelling the
		// operation

		// 10 seconds timeout
		return (time(NULL) - m_start) >= 10;  // seconds
	}

	void resetTimeOut()
	{
		// Called at the beginning of an operation (eg. connecting,
		// a read() or a write() on a socket...)
		m_start = time(NULL);
	}

	bool handleTimeOut()
	{
		// If isTimeOut() returned true, this function will be called. This
		// allows you to interact with the user, ie. display a prompt to
		// know whether he wants to cancel the operation.

		// If you return false here, the operation will be actually cancelled.
		// If true, the time out is reset and the operation continues.
		return false;
	}

private:

	time_t m_start;
};

class timeoutHandlerFactory : public vmime::net::timeoutHandlerFactory
{
public:

	vmime::shared_ptr <vmime::net::timeoutHandler> create()
	{
		return vmime::make_shared <timeoutHandler>();
	}
};

/** Tracer used to demonstrate logging communication between client and server.
*/

class myTracer : public vmime::net::tracer
{
public:

	myTracer(vmime::shared_ptr <std::ostringstream> stream,
		vmime::shared_ptr <vmime::net::service> serv, const int connectionId)
		: m_stream(stream), m_service(serv), m_connectionId(connectionId)
	{
	}

	void traceSend(const vmime::string& line)
	{
		*m_stream << "[" << m_service->getProtocolName() << ":" << m_connectionId
			<< "] C: " << line << std::endl;
	}

	void traceReceive(const vmime::string& line)
	{
		*m_stream << "[" << m_service->getProtocolName() << ":" << m_connectionId
			<< "] S: " << line << std::endl;
	}

private:

	vmime::shared_ptr <std::ostringstream> m_stream;
	vmime::shared_ptr <vmime::net::service> m_service;
	const int m_connectionId;
};

class myTracerFactory : public vmime::net::tracerFactory
{
public:

	myTracerFactory(vmime::shared_ptr <std::ostringstream> stream)
		: m_stream(stream)
	{
	}

	vmime::shared_ptr <vmime::net::tracer> create
	(vmime::shared_ptr <vmime::net::service> serv, const int connectionId)
	{
		return vmime::make_shared <myTracer>(m_stream, serv, connectionId);
	}

private:

	vmime::shared_ptr <std::ostringstream> m_stream;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



#define _URL "smtp://andriy.a.andreyev:bf17041997@smtp.gmail.com:587"
#define _FROM "andriy.a.andreyev@gmail.com"
#define _TO_1 "andrey.andreyev@outsource-to-ukraine.com"
#define _TO_2 "2pi14b.andreiev@gmail.com"
#define _USER "andriy.a.andreyev"
#define _PASSWORD "bf17041997"
#define _SUBJECT "Test letter"
#define _FILE_PATH "/home/andriy/Documents/attach.txt" //full path!
#define _MESSAGE u"Hello!\n\rIt is test letter from my program!\r\n∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯∯\r\n潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮潮\r\nWith best regards,\r\n Andriy Andreyev!"

std::u16string utf8_to_utf16(const std::string &s) {
	std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> convert;
	return convert.from_bytes(s);
}

std::string utf16_to_utf8(const std::u16string &s) {
	std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> convert;
	return convert.to_bytes(s);
}


// Global session object
static vmime::shared_ptr <vmime::net::session> g_session = vmime::net::session::create();

//// Exception helper
//static std::ostream& operator<<(std::ostream& os, const vmime::exception& e)
//{
//	os << "* vmime::exceptions::" << e.name() << std::endl;
//	os << "    what = " << e.what() << std::endl;
//
//	// More information for special exceptions
//	if (dynamic_cast <const vmime::exceptions::command_error*>(&e))
//	{
//		const vmime::exceptions::command_error& cee =
//			dynamic_cast <const vmime::exceptions::command_error&>(e);
//
//		os << "    command = " << cee.command() << std::endl;
//		os << "    response = " << cee.response() << std::endl;
//	}
//
//	if (dynamic_cast <const vmime::exceptions::invalid_response*>(&e))
//	{
//		const vmime::exceptions::invalid_response& ir =
//			dynamic_cast <const vmime::exceptions::invalid_response&>(e);
//
//		os << "    response = " << ir.response() << std::endl;
//	}
//
//	if (dynamic_cast <const vmime::exceptions::connection_greeting_error*>(&e))
//	{
//		const vmime::exceptions::connection_greeting_error& cgee =
//			dynamic_cast <const vmime::exceptions::connection_greeting_error&>(e);
//
//		os << "    response = " << cgee.response() << std::endl;
//	}
//
//	if (dynamic_cast <const vmime::exceptions::authentication_error*>(&e))
//	{
//		const vmime::exceptions::authentication_error& aee =
//			dynamic_cast <const vmime::exceptions::authentication_error&>(e);
//
//		os << "    response = " << aee.response() << std::endl;
//	}
//
//	if (dynamic_cast <const vmime::exceptions::filesystem_exception*>(&e))
//	{
//		const vmime::exceptions::filesystem_exception& fse =
//			dynamic_cast <const vmime::exceptions::filesystem_exception&>(e);
//
//		os << "    path = " << vmime::platform::getHandler()->
//			getFileSystemFactory()->pathToString(fse.path()) << std::endl;
//	}
//
//	if (e.other() != NULL)
//		os << *e.other();
//
//	return os;
//}


void PrintLog(int _step, std::string _entrie = "")
{
	std::cout << "Step " << _step << ": ";
	if (!_entrie.empty())
	{
		std::cout << _entrie;
	}
	std::cout << std::endl;
}

vmime::messageBuilder init()
{
	// VMime initialization
	vmime::platform::setHandler<vmime::platforms::posix::posixHandler>();

	try
	{
		vmime::messageBuilder  mb;

		// Fill in the basic fields
		mb.setExpeditor(vmime::mailbox(_FROM));

		//TO & CC
		vmime::addressList to;
		to.appendAddress(vmime::make_shared <vmime::mailbox>(_TO_1));
		//to.appendAddress(vmime::make_shared <vmime::mailbox>(_TO_2));
		mb.setRecipients(to);

		//BCC
		vmime::addressList bcc;
		bcc.appendAddress(vmime::make_shared <vmime::mailbox>(_TO_2));
		mb.setBlindCopyRecipients(bcc);

		mb.setSubject(vmime::text(_SUBJECT));

		// Message body
		mb.getTextPart()->setCharset("UTF-16");
		mb.getTextPart()->setText(vmime::make_shared <vmime::stringContentHandler>(utf16_to_utf8(_MESSAGE).c_str()));

		// Adding an attachment
		auto a = vmime::make_shared <vmime::fileAttachment>
			(
				_FILE_PATH,                               // full path to file
				vmime::mediaType("text/plain"),           // content type
				vmime::text("My first attachment")        // description
				);
		a->getFileInfo().setFilename("attach.txt");
		a->getFileInfo().setCreationDate(vmime::datetime("17 Apr 1997 14:30:00 +0200"));

		mb.attach(a);

		// Construction
		//for logs
		//auto msg = mb.construct();
		//vmime::string dataToSend = msg->generate();
		//std::cout << dataToSend << std::endl;

		return std::move(mb);

	}
	// VMime exception
	catch (vmime::exception& e)
	{
		PrintLog(0, "vmime::exception: " + std::string(e.what()));
		throw;
	}
	// Standard exception
	catch (std::exception& e)
	{
		PrintLog(0, "std::exception: " + std::string(e.what()));
		throw;
	}

}


static void sendMessage()
{
	try
	{
		// Request user to enter an URL
		// Step 1: setting URL (host, user, pass)
		PrintLog(1, "setting URL");
		vmime::utility::url url(_URL);

		vmime::shared_ptr <vmime::net::transport> tr;

		if (url.getUsername().empty() || url.getPassword().empty())
		{
			// Step 2: don't use authorisation
			PrintLog(2, "don't use authorisation");
			tr = g_session->getTransport(url, vmime::make_shared <interactiveAuthenticator>());
		}
		else
		{
			// Step 2: use authorisation
			PrintLog(2, "use authorisation");
			tr = g_session->getTransport(url);
		}

#if VMIME_HAVE_TLS_SUPPORT

		// Step 3: enable TLS support
		PrintLog(3, "enable TLS support");
		tr->setProperty("connection.tls", true);
		//tr->setProperty("transport.smtp.options.need-authentication", true);

		// Step 4: set the time out handler
		PrintLog(4, "set the time out handler");
		tr->setTimeoutHandlerFactory(vmime::make_shared <timeoutHandlerFactory>());


		// Step 5: // Set the object responsible for verifying certificates, in the
		// case a secured connection is used (TLS/SSL)
		PrintLog(5, "set the object responsible for verifying certificates");
		tr->setCertificateVerifier(vmime::make_shared <interactiveCertificateVerifier>());

#endif // VMIME_HAVE_TLS_SUPPORT

		// You can also set some properties (see example7 to know the properties
		// available for each service). For example, for SMTP:
		if (!url.getUsername().empty() || !url.getPassword().empty())
		{
			tr->setProperty("options.need-authentication", true);
		}

		//// Trace communication between client and server
		//vmime::shared_ptr <std::ostringstream> traceStream = vmime::make_shared <std::ostringstream>();
		//tr->setTracerFactory(vmime::make_shared <myTracerFactory>(traceStream));

		// Step 6: connect to server
		PrintLog(6, "connect to server");
		tr->connect();


		// Send the message
		// Step 7: send the message
		PrintLog(7, "send the message");
		auto msg_test = init();
		tr->send(msg_test.construct());

		// Step 8: disconnect
		PrintLog(8, "disconnect");
		tr->disconnect();
	}
	catch (vmime::exception& e)
	{
		PrintLog(0, "vmime::exception: " + std::string(e.what()));
		throw;
	}
	catch (std::exception& e)
	{
		PrintLog(0, "std::exception: " + std::string(e.what()));
		throw;
	}
}


int main()
{
	sendMessage();
	return 0;
}
